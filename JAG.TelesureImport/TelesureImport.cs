﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using Selenium;
using System.Web;
using System.Text;
using System.Net;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Threading;
using System.Net.Mail;
using System.IO;

namespace JAG.TelesureImport
{
    public class TelesureImport
    {
        private IWebDriver		driver;
        private String			browser, version, os;
		public enum FormType	{ STI, LIFE };
		
        public				TelesureImport()
        {
            this.browser = "chrome";
            this.version = "53";
            this.os = "Windows 10";
        }
 
        public void			Init()
        {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.SetCapability(CapabilityType.BrowserName, browser);
            caps.SetCapability(CapabilityType.Version, version);
            caps.SetCapability(CapabilityType.Platform, os);
            caps.SetCapability("name", TestContext.CurrentContext.Test.Name);
 			try {																											  
				driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), caps, TimeSpan.FromSeconds(42));
			} catch (Exception e) {
				Console.WriteLine(e);
			}
        }

		/**
		 *  AUTHENTICATION
		 */
		public void			Login()
		{
			IWebElement		username;
			IWebElement		password;

			driver.Navigate().GoToUrl("http://upstream.telesure.co.za");
			driver.Navigate().GoToUrl("javascript:document.getElementById('logoutForm').submit()");
			username = driver.FindElement(By.Id("UserName"));
			password = driver.FindElement(By.Id("Password"));
			username.SendKeys("Jag");
			password.SendKeys("th3t1m31s@n0w");
			password.Submit();
		}

		public void Logout()
		{
			ReadOnlyCollection<IWebElement>	links;

			links = driver.FindElements(By.TagName("a"));
			foreach (IWebElement link in links) {
				if (link.Text == "Log off" && link.Displayed == true) {
					link.Submit();
					break;
				}
			}
		}

		public List<int>	whichSelectElements(FormType form)
		{
			List<int>		selects;
			
			selects = new List<int>();
			if (form == FormType.STI) {
				selects.Add(0);
				selects.Add(1);
			}
			else {
				selects.Add(2);
				selects.Add(3);	
			}
			return (selects);
		}

		/**
		 * FILLERS
		 */
		public void			DateFiller(string dateElementId)
		{
			string							selectedDate;
			IWebElement						dateElement;  
			DateTime						toDay, firstDay;
			ReadOnlyCollection<IWebElement>	applyBtns;

			dateElement = driver.FindElement(By.Id(dateElementId));	
			toDay = DateTime.Now;
			firstDay = new DateTime(toDay.Year, toDay.Month, 1);
			selectedDate = firstDay.ToString("MM/dd/yyyy") + " - " + toDay.ToString("MM/dd/yyyy");
			dateElement.SendKeys(selectedDate);

			// Click apply to remove calendar from screen 
			applyBtns = driver.FindElements(By.ClassName("applyBtn"));
			foreach (IWebElement btn in applyBtns) {
				if (btn.Displayed == true) {
					btn.Click();
					break ;
				}
			}
		}

		public void			SelectFiller(FormType form)
		{
			ReadOnlyCollection<IWebElement>	selectElements;
			SelectElement					vdn, report;
			List<int>						selects;

			selectElements = driver.FindElements(By.TagName("select"));
			selects = whichSelectElements(form);
			vdn = new SelectElement(selectElements[selects[0]]);
			report = new SelectElement(selectElements[selects[1]]);	
			vdn.SelectByValue("All");
			report.SelectByValue("premium");
		}

		public void			RunAndDownloadReport(string downloadLink)
		{
			ReadOnlyCollection<IWebElement> buttons;
			IWebElement						download;

			buttons = driver.FindElements(By.TagName("button"));
			foreach (IWebElement btn in buttons) {
				if (btn.Text == "Run Report") {
					btn.Click();
					break;
				}
			}
			download = driver.FindElement(By.CssSelector(downloadLink));
			// Wait, or my mom will shoot!
			while (download.Displayed == false)
				Thread.Sleep(4200);	 // worst game loop ever
			download.Click();

		}

		public void			FillData(FormType form)
		{
			string		downloadLink, dateElementId;

			// different section changes dataset
			dateElementId = (form == FormType.STI) ? "reservation" : "lifeDateSelection";
			downloadLink = (form == FormType.STI) ? "[href*='http://upstream.telesure.co.za/Reports/STI_premium.xls']"  :
													"[href*='http://upstream.telesure.co.za/Reports/Life_premium.xls']";
			DateFiller(dateElementId);
			SelectFiller(form);
			RunAndDownloadReport(downloadLink);
		}

		/**
		 * Main Executable
		 */
		public void			TelesureJob()
		{
			IWebElement	life;

			Init();
			Login();
			FillData(FormType.STI);

			// Go to "Life" section
			life = driver.FindElement(By.CssSelector("[href*='#life']"));
			life.Click();

			FillData(FormType.LIFE);
			Logout(); 
			CleanUp();	
			SendMail("NO PARK", "WU TANG CLAN");
		}

		/**
		 * Send Mail using GMAIL server
		 */
		public void			SendMail(string subject, string body)
		{
			MailAddress		from, to;
			MailMessage		msg;
			SmtpClient		smtp;
			const string	user = "gk3kana", pwd = "";
			
			// Setup smtp client with gmail smtp
			from = new MailAddress("gontse@jagmethod.com", "Gontse Kekana");
			to = new MailAddress("gontsekekana@yandex.com", "gontse@yandex");
			smtp = new SmtpClient()
			{
				Host = "smtp.gmail.com",
				Port = 587,
				EnableSsl = true,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(from.Address, pwd)
			};
			msg = new MailMessage(from, to) {
				Subject = subject,
				Body = body
			};

			// Add required attachments	and SEND
			string			f1, f2;

			f1 = @"C:\\Users\\" + user + "\\Downloads\\Life_premium.xls";
			f2 = @"C:\\Users\\" + user + "\\Downloads\\STI_premium.xls";
			using (Attachment a1 = new Attachment(f1), a2 = new Attachment(f2)) {
				msg.Attachments.Add(a1);
				msg.Attachments.Add(a2);
				smtp.Send(msg);
			}

			// expunge files
			if (File.Exists(f1))
				File.Delete(f1);
			if (File.Exists(f2))
				File.Delete(f2);
		}
 
        public void CleanUp()
		{
			driver.Quit();
		}
    }
}
